#!/usr/bin/env bash

PtoInstall="bs4 icalendar pytz"
AtoInstall="python3 python3-pip"
red=$(tput setaf 1)
green=$(tput setaf 2)
toend=$(tput hpa $(tput cols))$(tput cub 10)

privilege () {
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be run as root!" 1>&2;
        exit 1;
    fi
}

audit () {
    if [[ "$auditCode" -eq "0" ]];then
        echo -en "${toend}${green}[ Success ]$(tput sgr0)";
    else
        echo -en   "${red}${toend}[ Failed! ]$(tput sgr0)";
    fi
}

privilege;

echo -n  "Update apt source:";
apt update  > /dev/null 2>&1
audit;

echo $AtoInstall | tr ' ' '\n' | while read REPLAY; do PACKAGE=$REPLAY;
    apt install $PACKAGE > /dev/null 2>&1;
    auditCode=$?;
    echo -n  " apt install package: $PACKAGE";
    audit;
done


echo $PtoInstall | tr ' ' '\n' | while read REPLAY; do PACKAGE=$REPLAY;
    pip3 install $PACKAGE > /dev/null 2>&1;
    auditCode=$?;
    echo -n  "pip3 install package: $PACKAGE";
    audit;
done




