#!/usr/bin/env python3
#!/usr/bin/python3

import os

from urllib.request import urlopen
from bs4 import BeautifulSoup
from icalendar import Calendar, Event
from calendar import month_name
from datetime import datetime
from uuid import uuid4 as uid
from os import utime
from pytz import timezone as tzone

folder_for_ics_file = "./"
path_to_config_file = "./config.txt"
#timezone = 'America/Indiana/Marengo'
#timezone = 'Etc/GMT-6'
timezone = 'US/Central'


# CHECKED
def date_time_parse(date, time):
    tmp_date = date.split(",")
    year = int(tmp_date[2].strip())
    month = str(list(month_name).index(tmp_date[1].split(" ")[1]))
    if len(month) < 2:
        month = '0' + month
    day = int(tmp_date[1].split(" ")[2])
    month = int(month)
    #print("time = ", time)
    tmp_time = list(map(int, str(datetime.strptime(time.strip(), '%I:%M %p')).split(" ")[1].split(':')))
    date_time = datetime(year, month, day, tmp_time[0], tmp_time[1], tmp_time[2], tzinfo=tzone(timezone))
    return date_time


# CHECKED
def get_soup(url):
    tmp_url = url.split(",")
    html = urlopen(tmp_url[0]).read()
    soup = BeautifulSoup(html, 'html.parser')
    return (soup, url)


# CHECKED
def get_name_for_file(soup, url):
    try:
        name = soup[0].find_all('span', {"class": "SubHeading", "id": "ctl00_MainBody_TeamName"})[0].text
    except IndexError:
        try:
            name = soup[0].find_all('div', {"class": "PageHeading"})[0].text
        except IndexError:
            try:
                name = soup[1].split("?")[1]
            except IndexError:
                name = soup[1].replace("/", "_").replace("\\","_")
    configName = url.split(",")
    if len(configName) > 1:
      name = name + configName[1]

    return name.replace(" ", "_") + ".ics"


def parse_html(soup,url,name_of_file):
    events = soup[0].find_all('table', {"border": "0", "cellpadding": "10", "cellspacing": "0"})
    ics_events_list = []
    for i in range(len(events)):
        date = events[i].find_next("th").text
        for j in events[i].find_all("tr"):
            if len(j.find_all('div', {"class": "GameNumber"})) < 1:
                continue
            MatchTime = j.find_next("div", {"class": "MatchTime"}).text
            ics_date_time = date_time_parse(date, MatchTime)
            ics_location = j.find_next("td", {"class": "location"}).find_next("a").text
            tmp_score = j.find_all("span", {"class": "score"})
            GameNumber = j.find_next("div", {"class": "GameNumber"}).text
            homeTeam = j.find_next("td", {"class": "homeTeam"}).find_next("a").text
            awayTeam = j.find_next("td", {"class": "awayTeam"}).find_next("a").text
            homeTeamScore = tmp_score[0].text
            awayTeamScore = tmp_score[1].text
            #ics_name = GameNumber + " " + homeTeam + " ( " + homeTeamScore + " : " + awayTeamScore + ")  " + awayTeam
            ics_name = GameNumber + " " + awayTeam + " at " + homeTeam
            ics_events_list.append([ics_name, ics_date_time, ics_location])
         
            # Get description notes if it exists 
            if os.path.isfile(folder_for_ics_file + name_of_file+"_notes.txt"):
                with open(folder_for_ics_file + name_of_file+"_notes.txt", 'r') as myfile:
                    notes = "\n\nNotes:\n"+myfile.read()
            else:
                notes = ""

    c = Calendar()
    for event in ics_events_list:
        e = Event()
        e.add('summary', event[0])
        e.add('description', url + notes)
        e.add('uid', str(uid()))
        e.add('dtstart', event[1])
        e.add('location', event[2])
        c.add_component(e)
    return c #c.walk('vevent')  #c.subcomponents # c.to_ical()


# CHECKED
def ics_read(filename):
    path =  filename
    ics_file_str = ""
    try:
        with open(path, 'r') as f:
            ics_file_str = f.read()
    except FileNotFoundError:
        with open(path, 'a'):
            utime(path, None)
    if ics_file_str == "":
        return Calendar()
    return Calendar.from_ical(ics_file_str)


# CHECKED
def ics_write(calendar, filename):
    with open(filename, 'wb') as f:
        f.write(calendar.to_ical())

# TODO: REWORK
def get_events_summary(events):
    summary = []
    for i in range( len(events)):
        summary.append(events[i]['SUMMARY'])
    return summary

# TODO: REWORK
def join_calendars(p_events, s_events):
    p = get_events_summary(p_events)
    s = get_events_summary(s_events)
    print(s)
    for k, i in enumerate(s):
        if i not in p:
            p_events.append(s_events[k])
    return p_events

# CHECKED
def read_config(path):
    links = []
    with open(path, 'r') as f:
        draft_links = list(f)
    for i in draft_links:
        without_n = i.strip("\n")
        if not (without_n == "" or without_n.isspace() or without_n is None):
            links.append(without_n)
    return links


def main():
    config = read_config(path_to_config_file)
    for i in config:
        soup = get_soup(i)
        name_of_file = get_name_for_file(soup,i)
        path_to_ics_file = folder_for_ics_file + name_of_file
        prime = ics_read(path_to_ics_file)
        second = parse_html(soup,i,name_of_file)
#        print(join_calendars(prime, second))
        #to_write = join_calendars(prime, second)
        #ics_write(to_write, path_to_ics_file)
        ics_write(second, path_to_ics_file)
        #print(second)
        print("Script processed URL successfully: ", i, " ", name_of_file)


if __name__ == "__main__":
    main()
