# Install on Ubuntu
* chmod +x install_env.sh gotsport_to_icalendar.py
* sudo ./install_env.sh
* open file gotsport_to_icalendar.py in text editor:
  * In line 9 change path to folder where you like to save icalendar file: folder_for_ics_file = "./"
  * In line 10 change path to your config file: path_to_config_file = "./config.txt"

# Usage
* insert in the config file all the links that need to parse
* run script: ./gotsport_to_icalendar.py
